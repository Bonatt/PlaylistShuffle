# Very Bad Playlist Shuffle Algorithm

This thought experiment was in context of a very bad playlist shuffle algorithm.

Given a music playlist of `n` songs, select one song at random.
For the initial selection all songs have equal selection probability.
For subsequent selections the selection probability of the selected song increases
by some constant `e`. The probability density function (PDF) (really, the
probability mass function (PMF)) is then renormalized. 

After how many selections will the next song guaranteed to be the previous song? 
I.e., at what iteration does a song singularity occur?

Of course this may be solved analytically but I'll solve this via iterative simulation.
Below are the PMF and final probabilities for single simulation 
with convergence in approximately `i = 270` iterations.

<!--[](Results/pmf_e=0.1_n=10.gif) ![](Results/probability_per_iteration_e=0.1_n=10.png)-->
<p float="left">
  <img src="Results/pmf_e=0.1_n=10.gif" width="444" /> 
  <img src="Results/probability_per_iteration_e=0.1_n=10.png" width="444" />
</p>


One can repeat the above process `N` times. Below is the resultant histogram.
![](Results/counts_per_iteration_e=0.1_n=10_N=10000.png)

Poisson-y, right? What's the expectation value?
Other analysis, implementation to come. Had some fun for a few good hours, at least.
