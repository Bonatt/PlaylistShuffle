import numpy as np
import matplotlib.pyplot as plt



# Given a music playlist of n songs, select one song at random. 
# For the initial selection all songs have equal probability to be selected.
# For subsequent selections the probability of the song picked increases 
# by some constant e. The probability density function (pdf) really, the 
# probability mass function (pmf)) is renormalized. After how many selections 
# will the next song guaranteed to be the previous song? This thought 
# experiment was in context of a very bad playlist shuffle algorithm.


# Below:
# Plot a live plot of the PMF. Convert to gif via, e.g.,
# convert -delay 10 -loop 0 Results/PMF/*.png Results/pmf_e=0.1_n=10.gif
# Plot of the values per iteration.
# Histogram of above over N iterations
# I also want to look at integral, derivative of each value per iteration.
# And number of values "in the running" per iteration.

def init_pmf(n=10, e=None, p0=None, pmax=0.999):
    '''Probability Mass Function'''
    if not e:
        e = 1/n
    if not p0:
        p0 = 1/n
    values = range(n)
    pmf0 = np.ones(n)/n
    #f = e/(n-1)
    #ef = e + f
    return n, e, p0, pmax, values, pmf0

def init_plot(n, pmf):
    fig, ax = plt.subplots()
    data = ax.plot(pmf.copy())[0]
    plt.xticks(np.arange(0, n, 1))
    plt.yticks(np.arange(0, 1.1, 0.1)) #0.05
    plt.xlabel('Value')
    plt.ylabel('Probability')
    plt.grid()
    return fig, ax, data

def plot_pmf(fig, ax, data, pmf, i, e):
    data.set_ydata(pmf)
    for t in ax.texts:
        t.set_visible(False)
    for x,y in enumerate(pmf):
        text = ax.text(x, y, str(round(y,3)))
    ax.set_ylim(0, max(pmf)+e)
    plt.title('PMF; e={}, n={}, i={}'.format(e, n, str(i).zfill(4)))
    plt.pause(0.0001)

def iterate_pmf(p, pmax, e, values, pmf, plot=True, n=10):
    if plot:
        fig, ax, data = init_plot(n, pmf)
    i, PMF = 0, []
    while p < pmax:
        value = np.random.choice(values, p=pmf)
        #pmf -= f
        #pmf[song] += ef
        #pmf[pmf<0] = 0
        #pmf += (1-sum(pmf))/n
        pmf[value] += e
        pmf /= np.sum(pmf)
        PMF.append(pmf.copy())
        p = np.max(pmf)
        i += 1
        if plot:
            plot_pmf(fig, ax, data, pmf, i, e)   
            plt.savefig('Results/PMF/pmf_e={}_n={}_i={}.png'.format(e, n, str(i).zfill(4))) 
    if plot:
        #plt.savefig('Results/pmf_e={}_n={}_i={}.png'.format(e, n, i))
        return PMF#, fig, ax, data
    return PMF


n, e, p0, pmax, values, pmf0 = init_pmf(n=10)
p, pmf = p0, pmf0.copy()
PMF = iterate_pmf(p, pmax, e, values, pmf, plot=True, n=n)
PMF = np.array(PMF)


# I'll eventually want to add this to the plot above.
plt.figure()
x = range(PMF.shape[0])
for e_,y in enumerate(PMF.T):
    plt.plot(x, y, label=str(e_))
plt.yticks(np.arange(0, 1.1, 0.1))
plt.xlabel('Iteration')
plt.ylabel('Probability')
plt.title('Probability Per Iteration; e={}, n={}'.format(e, n))
plt.legend(title='Value')
plt.grid()
plt.savefig('Results/probability_per_iteration_e={}_n={}.png'.format(e, n))
plt.show(block=False)




'''
def loop(n=10, N=100, e=None, p0=None, pmax=0.999):
    n, e, p0, pmax, values, pmf0 = init_pmf(n=n)
    Niterations = []    
    for i in range(N):
        p, pmf = p0, pmf0.copy()
        PMF = iterate_pmf(p, pmax, e, values, pmf, plot=False)
        Niterations.append(len(PMF))
    return np.array(Niterations)


N=10000
Niterations = loop(n=n, N=N)

plt.figure()
binwidth = 10
bins = range(0, round(max(Niterations)+binwidth,-2), binwidth)
hist, bin_edges = np.histogram(Niterations, bins=bins)#bins=100)
plt.bar(bin_edges[:-1], hist, width=binwidth, align='edge', edgecolor='w')#hist.shape[0])
plt.xlim(min(bin_edges), max(bin_edges))
#yticks = range(0, max(hist)+10, 10)
#plt.yticks(yticks)
plt.xlabel('Iterations')
plt.ylabel('Counts/{} iterations'.format(binwidth))
plt.title('Iterations Until Singularity; e={}, n={}, N={}'.format(e, n, N))
plt.grid()
plt.savefig('Results/counts_per_iteration_e={}_n={}_N={}.png'.format(e, n, N))
plt.show(block=False)
# Eventually fit, etc. above.
'''
